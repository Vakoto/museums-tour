package main

const (
	port     = ":50000"
	grpcAddr = ":50001"
	keySize  = 1024
	fileName = "museums.csv"
	consulAddress = "127.0.0.1:8500"
)
