package main

import (
	"bufio"
	"encoding/csv"
	"gitlab.com/Vakoto/museums-tour-tools/model"
	"io"
	"os"
	"strconv"
)

func parse(fileName string) ([]*model.Museum, error) {
	var empty []*model.Museum

	file, err := os.Open(fileName)
	if err != nil {
		return empty, err
	}

	reader := csv.NewReader(bufio.NewReader(file))
	reader.Comma = ';'

	_, err = reader.Read()
	if err != nil {
		return empty, err
	}

	var museums []*model.Museum
	for {

		line, err := reader.Read()
		if err != nil {
			if err == io.EOF {
				break
			} else {
				return empty, err
			}
		}

		id, err := strconv.Atoi(line[0])
		if err != nil {
			return empty, err
		}

		name := line[1]

		latitude, err := strconv.ParseFloat(line[2], 64)
		if err != nil {
			return empty, err
		}

		longitude, err := strconv.ParseFloat(line[3], 64)
		if err != nil {
			return empty, err
		}

		address := line[4]
		phone := line[5]
		contacts := line[6]

		museums = append(museums, &model.Museum{
			Id:        int32(id),
			Name:      name,
			Latitude:  latitude,
			Longitude: longitude,
			Address:   address,
			Phone:     phone,
			Contacts:  contacts,
		})
	}

	return museums, nil
}
