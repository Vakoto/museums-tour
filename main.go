package main

import (
	"context"
	"crypto/rsa"
	"fmt"
	consul "github.com/hashicorp/consul/api"
	"github.com/phayes/freeport"
	"gitlab.com/Vakoto/museums-tour-tools/model"
	"gitlab.com/vakoto/museums-tour-tools/health_check"
	"google.golang.org/grpc"
	"google.golang.org/grpc/balancer/roundrobin"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/peer"
	"google.golang.org/grpc/reflection"
	"google.golang.org/grpc/status"
	"log"
	"math/rand"
	"net"
	"os"
	"os/signal"
	"strconv"
	"sync"
	"syscall"
	"time"
)

var (
	privateKey       *rsa.PrivateKey
	publicKeyJSON    []byte
	museums          []*model.Museum
	museumsMapConfig map[int32]*model.Museum
)

type server struct {
	mu        sync.Mutex
	statusMap map[string]grpc_health_v1.HealthCheckResponse_ServingStatus
}

func NewServer() *server {
	statusMap := map[string]grpc_health_v1.HealthCheckResponse_ServingStatus{
		"grpc.health.v1.Health": grpc_health_v1.HealthCheckResponse_SERVING,
	}
	return &server{
		statusMap: statusMap,
	}
}

func (s *server) Check(ctx context.Context, in *grpc_health_v1.HealthCheckRequest) (*grpc_health_v1.HealthCheckResponse, error) {
	s.mu.Lock()
	defer s.mu.Unlock()
	if in.Service == "" {
		return &grpc_health_v1.HealthCheckResponse{
			Status: grpc_health_v1.HealthCheckResponse_SERVING,
		}, nil
	}
	if status, ok := s.statusMap[in.Service]; ok {
		return &grpc_health_v1.HealthCheckResponse{
			Status: status,
		}, nil
	}
	return nil, status.Error(codes.NotFound, "unknown service")
}

func (s *server) GetPath(ctx context.Context, in *model.GetPathRequest) (*model.GetPathReply, error) {
	if peer, ok := peer.FromContext(ctx); ok {
		log.Printf("Path find request from %s", peer.Addr.String())
	}

	config := consul.DefaultConfig()
	config.Address = consulAddress
	consulClient, err := consul.NewClient(config)

	services, _, err := consulClient.Health().Service("solver", "", true, nil)
	if err != nil {
		log.Printf("cant get alive services")
		return nil, err
	}

	addresses := make([]string, 0)
	for _, item := range services {
		addr := item.Service.Address + ":" + strconv.Itoa(item.Service.Port)
		addresses = append(addresses, addr)
	}

	if len(addresses) == 0 {
		log.Printf("Failed to find healthy solver services")
		return nil, status.Error(codes.NotFound, "Solver service not found")
	}
	log.Printf("Received solver services: %v", addresses)

	rand.Seed(time.Now().Unix())
	randomIndex := rand.Intn(len(addresses))
	grpcAddr := addresses[randomIndex]
	grpcConn, err := grpc.Dial(
		grpcAddr,
		grpc.WithInsecure(),
		grpc.WithBalancerName(roundrobin.Name))
	if err != nil {
		log.Printf("Failed to dial: %v", err)
		return nil, err
	}
	defer grpcConn.Close()

	c := model.NewPathFinderClient(grpcConn)
	if err != nil {
		log.Printf("Can't connect to grpc")
		return nil, err
	}
	defer grpcConn.Close()

	ctx, cancel := context.WithTimeout(context.Background(), time.Minute)
	defer cancel()
	// TODO: peer
	//ctx = peer.NewContext(	ctx, &peer.Peer{
	//	Addr: "127.0.0.1,
	//})
	log.Printf("[%s] GRPC: sent request", grpcAddr)
	reply, err := c.GetPath(ctx, &model.GetPathRequest{
		MuseumIds: in.MuseumIds,
	})
	if err != nil {
		log.Printf("[%s] GRPC: could not get reply: %v", grpcAddr, err)
		return nil, err
	}
	log.Printf("[%s] GRPC: response content: Museums: %v\tDistance: %v", grpcAddr, reply.Path, reply.Distance)

	return reply, err
}

func (s *server) FetchMuseums(ctx context.Context, in *model.GetMuseumsRequest) (*model.GetMuseumsReply, error) {
	if peer, ok := peer.FromContext(ctx); ok {
		log.Printf("FetchMuseums request from %s", peer.Addr.String())
	}

	return &model.GetMuseumsReply{ Museums: museums }, nil
}


func main() {
	args := os.Args
	if (len(args) < 2) {
		log.Fatalf("First argument should be the server address")
	}
	address := args[1]

	var err error
	museums, err = parse(fileName)
	if err != nil {
		log.Fatalf("CSV: failed to parse file: %s", err)
	}
	museumsMapConfig = make(map[int32]*model.Museum)
	for _, museum := range museums {
		museumsMapConfig[museum.Id] = museum
	}

	port, err := freeport.GetFreePort()

	listener, err := net.Listen("tcp", fmt.Sprintf(":%d", port))
	if err != nil {
		log.Fatalf("Failed to listen: %s", err)
	}
	defer listener.Close()
	log.Printf("Listening on :%d", port)

	server := NewServer()
	s := grpc.NewServer()
	model.RegisterMuseumDispatcherServer(s, server)
	grpc_health_v1.RegisterHealthServer(s, server)

	config := consul.DefaultConfig()
	config.Address = consulAddress
	consulClient, err := consul.NewClient(config)
	if err != nil {
		log.Fatalf("Failed to connect to the consul server: %s", err)
	}

	serviceId := "dispatcher:" + address + ":" + strconv.Itoa(port)
	serviceName := "dispatcher"

	reg := &consul.AgentServiceRegistration{
		ID:      serviceId,
		Name:    serviceName,
		Port:    port,
		Address: address,
		Checks: consul.AgentServiceChecks{
			&consul.AgentServiceCheck{
				CheckID:    "dispatcherHealthCheck:" + serviceId,
				Name:       "Dispatcher service health status",
				GRPC:       fmt.Sprintf("%s:%d", address, port),
				GRPCUseTLS: false,
				Interval:   "10s",
			},
		},
	}
	err = consulClient.Agent().ServiceRegister(reg)
	if err != nil {
		log.Fatalf("Failed to register dispatcher service: %s", err)
	}
	log.Printf("Service %s successfully registered", serviceId)

	defer func() {
		err := consulClient.Agent().ServiceDeregister(serviceId)
		if err != nil {
			log.Printf("Failed to deregister the dispatcher %s: %s", serviceId, err)
			return
		}
		log.Printf("Deregistered %s in consul", serviceId)
	}()

	reflection.Register(s)
	go func() {
		s.Serve(listener)
	}()

	signalChannel := make(chan os.Signal, 2)
	done := make(chan bool, 1)
	signal.Notify(signalChannel, os.Interrupt, syscall.SIGTERM)
	go func() {
		sig := <-signalChannel
		switch sig {
		case os.Interrupt, syscall.SIGTERM:
			done <- true
		}
	}()

	<- done
}
